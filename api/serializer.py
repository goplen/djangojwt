from rest_framework import serializers
from rest_framework_simplejwt.settings import api_settings
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework_simplejwt.views import TokenVerifyView

from api.models import User


class TokenRefreshSerializer(serializers.Serializer):
    refresh = serializers.CharField()
    type = serializers.CharField()

    def validate_token(self, attrs):
        refresh = RefreshToken(attrs)
        if api_settings.ROTATE_REFRESH_TOKENS:
            if api_settings.BLACKLIST_AFTER_ROTATION:
                try:
                    refresh.blacklist()
                except:
                    pass
        data = {"access_token": str(refresh.access_token), "refresh_token": str(refresh)}
        return data


class RegisterSerializer(serializers.Serializer):
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    email = serializers.EmailField()
    password = serializers.CharField()



class DeleteUser(serializers.Serializer):
    user_owner = serializers.CharField()


class UserSerializer(serializers.Serializer):
    email = serializers.CharField()
    password = serializers.CharField()

