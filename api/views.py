from abc import ABC
from datetime import timedelta

from django.db import IntegrityError
from django.shortcuts import render
from django.contrib.auth.hashers import check_password
from rest_framework import exceptions, status
from rest_framework.exceptions import NotFound
from rest_framework.permissions import IsAuthenticated, BasePermission
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.exceptions import TokenError, InvalidToken, AuthenticationFailed
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView, TokenObtainSlidingView, TokenViewBase
from rest_framework_simplejwt.tokens import AccessToken, RefreshToken

from api.models import User
from api.serializer import TokenRefreshSerializer, RegisterSerializer, UserSerializer


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)

        token['email'] = user.email
        if user.last_name != "" and user.first_name != "":
            token["lastName"] = user.last_name
            token["firstName"] = user.first_name

        return token


class RefreshTokenViews(APIView):
    def post(self, request):
        print(request.data)
        serializer = TokenRefreshSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            data = serializer.validate_token(serializer.validated_data.get("refresh"))
            return Response(data)


class RegisterNewUser(APIView):
    def post(self, request):
        serializer = RegisterSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            data = serializer.validated_data
            try:
                user = User.objects.create(
                    first_name=data.get("first_name"),
                    last_name=data.get("last_name"),
                    email=data.get("email")
                )
                user.set_password(data.get("password"))
                user.save()
                return Response({"success": "Успешная регистрация"}, status=status.HTTP_200_OK)
            except IntegrityError:
                return Response({"detail": "Такой пользователь уже существует"}, status=status.HTTP_400_BAD_REQUEST)


class DeleteUser(APIView):
    def post(self, request):
        pass


def get_user_with_token(access_token):
    access_token_obj = AccessToken(access_token)
    user_id = access_token_obj['user_id']
    user = User.objects.get(id=user_id)
    content = {'first_name': user.first_name, 'last_name': user.last_name, 'email': user.email}
    return Response(content)


class ProfileUserView(APIView):
    permission_classes = (IsAuthenticated,)

    def permission_denied(self, request, message=None, code=None):
        if request.authenticators and not request.successful_authenticator:
            raise exceptions.NotAuthenticated(detail="Отсутствует access_token", code=status.HTTP_400_BAD_REQUEST)
        raise exceptions.PermissionDenied(detail=message, code=status.HTTP_400_BAD_REQUEST)

    def get(self, request):
        data = request.auth
        return get_user_with_token(str(request.auth))

    serializer_class = TokenObtainPairSerializer


class MyTokenObtainPairView(APIView):
    def post(self, request):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            user = User.objects.filter(email=serializer.validated_data.get("email"))
            if user.exists():
                user = user.first()
                password = serializer.validated_data.get("password")
                if user.check_password(password):
                    refresh_token = RefreshToken.for_user(user)
                    access_token = AccessToken.for_user(user)
                    # access_token.set_exp(lifetime=timedelta(days=1))
                    return Response({"access_token": str(access_token),
                                     "refresh_token": str(refresh_token)}, status=status.HTTP_200_OK)
                else:
                    return Response({"detail": "Неверный логин или пароль"}, status=status.HTTP_400_BAD_REQUEST)
            return Response({"detail": "Пользователь не найден"}, status=status.HTTP_400_BAD_REQUEST)






