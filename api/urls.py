from django.urls import path
from rest_framework_simplejwt.views import TokenVerifyView
from api.views import MyTokenObtainPairView, RefreshTokenViews, RegisterNewUser, ProfileUserView

urlpatterns = [
    path('auth/login/', MyTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('verify/', TokenVerifyView.as_view(), name="verify_token"),
    #path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('auth/refresh/', RefreshTokenViews.as_view()),
    path('auth/register/', RegisterNewUser.as_view()),
    path('profile/', ProfileUserView.as_view())
]